import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable, Subject, of as ObservableOf } from 'rxjs';
import { map, shareReplay, takeUntil, tap, switchMap } from 'rxjs/operators';

import { deburr } from 'lodash';
import { Character, CharacterList } from 'src/app/models';

@Injectable({
  providedIn: 'root'
})
export class FuturamaService {

  readonly API_BASE_PATH = '/fandom';
  // readonly API_BASE_PATH = 'https://futurama.fandom.com';

  private forceReload$ = new Subject();
  private cachedList$: Observable<CharacterList>;

  constructor(private http: HttpClient) { }

  findCharacterByName(name: string): Observable<CharacterList> {

    if (!this.cachedList$) {

      const params: HttpParams = new HttpParams()
        .append('category', 'Characters')
        .append('limit', '1000');

      this.cachedList$ = this.http.get(`${this.API_BASE_PATH}/api/v1/Articles/List`, { params, responseType: 'json' })
        .pipe(
          map((data: CharacterListResponse) => {
            const characters = data.items.map(
              (item: Character) => ({
                ...item,
                searchIndex: deburr(item.title).toLowerCase()
              }));

            return ({
              characters,
              basePath: data.basepath,
            });
          }),
          takeUntil(this.forceReload$),
          shareReplay(1)
        );
    }

    const searchTerm = name.toLocaleLowerCase();

    return this.cachedList$.pipe(
      map((characterList: CharacterList) => {
        const filteredCharacters = characterList.characters
          .filter(character => character.searchIndex.includes(searchTerm))
          .sort((a, b) => a.searchIndex.indexOf(searchTerm) - b.searchIndex.indexOf(searchTerm));

        return filteredCharacters.slice(0, 10).map(character => character.id);
      }),

      switchMap(characterIds => {
        if (characterIds.length) {
          return this.getCharacterThumbnail(characterIds);
        }

        return ObservableOf(null);
      })
    );
  }

  clearCachedCharacterList(): void {
    this.forceReload$.next();
    this.cachedList$ = null;
  }

  private getCharacterThumbnail(characterIds: number[]): Observable<CharacterList> {

    const params: HttpParams = new HttpParams()
      .append('ids', characterIds.join(','))
      .append('abstract', '200')
      .append('width', '200')
      .append('height', '200');

    return this.http.get(`${this.API_BASE_PATH}/api/v1/Articles/Details`, { params, responseType: 'json' })
      .pipe(
        map((response: CharacterDetailResponse) => {
          const characters = Object.keys(response.items)
            .map(id => response.items[id]);

          return ({
            characters,
            basePath: response.basepath
          });
        }));
  }

}


interface CharacterListResponse {
  items: Character[];
  basepath: string;
}

interface CharacterDetailResponse {
  items: {
    [key: string]: Character
  };
  basepath: string;
}
