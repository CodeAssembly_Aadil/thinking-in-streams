import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable, of as ObservableOf } from 'rxjs';

import CountryData from '../assets/countries.json';

@Injectable({
  providedIn: 'root'
})
export class CountrySearchService {

  private countries: Array<string>;

  constructor(private httpClient: HttpClient) {
    this.countries = CountryData.map(country => country.name);
  }

  findCountry(query: string): Observable<string[]> {

    // ObservableFromEvent.

    const searchTerm = query.trim().toLowerCase();

    // return this.httpClient.get('src/assets/countries.json')

    // const results = this.countries
    //   .filter(country => country.toLowerCase().includes(searchTerm))
    //   .sort((a, b) => a.toLowerCase().indexOf(searchTerm) - b.toLowerCase().indexOf(searchTerm));

    this.httpClient.get('./countries.json').subscribe((response) => {
      console.log(response);
    });

    const results = this.countries.filter(country => country.toLowerCase().includes(searchTerm))
      .sort((a, b) => a.toLowerCase().indexOf(searchTerm) - b.toLowerCase().indexOf(searchTerm));



    // return ObservableOf(results).pipe(delay(3000));
    return ObservableOf(results);
  }

}
