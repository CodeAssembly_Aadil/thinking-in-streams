import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

// import { InputEvent } from 'dom-inputevent';

import { Observable, of as ObservableOf, Subject, timer } from 'rxjs';
import { distinctUntilChanged, map, debounceTime, switchMap, take, tap, shareReplay } from 'rxjs/operators';

import { FuturamaService } from 'src/app/futurama.service';
import { CharacterList } from 'src/app/models';
import value from '*.json';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

	searchControl: FormControl = new FormControl();

	// @ViewChild('searchInput', { read: ElementRef }) searchInput: ElementRef;

	// searchInputValue$: Observable<string> = ObservableOf(null);

	inputValue$: Observable<string>;
	debounced$: Observable<string>;
	trimmed$: Observable<string>;
	distinct$: Observable<string>;
	distinctComparisons$: Subject<{ x: string, y: string, distinct: boolean }> = new Subject();
	characterList$: Observable<CharacterList>;

	progress$: Observable<number>;

	searchResults$: Observable<CharacterList>;

	readonly debounceDelay = 1600;
	readonly increments = 10;

	constructor(private futuramaService: FuturamaService) { }

	ngOnInit(): void {

		// const searchInputValue$: Observable<string> = fromEvent(this.searchInput.nativeElement, 'input')
		//   .pipe(
		//     map((event: InputEvent) => (event.currentTarget as HTMLInputElement).value),
		//     map((value: string) => value.replace(/([^A-Za-z0-9\s])/, '')),
		//     tap((value: string) => {
		//       this.searchInput.nativeElement.value = value;
		//     })
		//   );

		// this.searchInputValue$.subscribe((v) => console.log('searchInputValue', v))

		this.inputValue$ = this.searchControl.valueChanges.pipe(shareReplay(1));

		// const searchInputValue$: Observable<string> = this.searchControl.valueChanges.pipe(
		//   map((value: string) => value.replace(/([^A-Za-z\s])/, '')),
		//   tap((value: string) => {
		//     this.searchControl.setValue(value, { emitEvent: false });
		//   })
		// );
		// const counter$ = new Subject<any>();

		// counter$.pipe(
		//   switchMap(() =>
		//     timer(0, 100).pipe(
		//       take(this.debounceDelay / 100),
		//       // map(value => value.emit(--options.count))
		//     )
		//   )
		// ).subscribe(value => {
		//   console.log(value);
		// });

		// counter$.next();

		this.debounced$ = this.inputValue$.pipe(debounceTime(this.debounceDelay));

		this.progress$ = this.inputValue$.pipe(
			switchMap(() =>
				timer(0, this.increments).pipe(
					take((this.debounceDelay / this.increments) + 1),
					map(count => {
						const total = (this.debounceDelay / this.increments);
						const remainder = total - count;
						return remainder / total;
					})
				)
			));

		this.trimmed$ = this.debounced$.pipe(map((value: string) => value.trim()));

		this.distinct$ = this.trimmed$.pipe(distinctUntilChanged((x, y) => {
			console.log(x, y)
			this.distinctComparisons$.next({ x, y, distinct: x !== y });

			return x === y;
		}));

		this.distinctComparisons$.subscribe(x => console.log(x))

		this.characterList$ = this.distinct$.pipe(switchMap((query) => {
			if (query.length > 2) {
				return this.futuramaService.findCharacterByName(query);
			}

			return ObservableOf(null);
		}));

		this.searchResults$ = this.characterList$;
	}

	searchResultsTrackBy(item: string) {
		return item;
	}


	onSearchInputChange() {
		// console.log(event);
	}

	setValue(): void {
		// this.searchControl.setValue('Bobs your uncle', { onlySelf: true, emitEvent: false, emitModelToViewChange: true, emitViewToModelChange: false });
		this.searchControl.setValue('Bobs your uncle', { emitEvent: false });

	}
}
