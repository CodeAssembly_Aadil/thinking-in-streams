export interface Character {
  title: string;
  id: number;
  url: string;
  searchIndex?: string;
  thumbnail?: string;
}
