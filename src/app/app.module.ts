import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { CodeViewComponent } from './code-view/code-view.component';
import { CharacterCardComponent } from './character-card/character-card.component';
import { GraphComponent } from './graph/graph.component';

@NgModule({
	imports: [
		BrowserModule,
		ReactiveFormsModule,
		HttpClientModule,
		FormsModule
	],
	declarations: [
		AppComponent,
		CodeViewComponent,
		CharacterCardComponent,
		GraphComponent
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
