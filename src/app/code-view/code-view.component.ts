import { Component, Input, OnInit, ViewChild, ElementRef } from '@angular/core';

import { CharacterList } from 'src/app/models';
import { StreamGate } from 'src/app/consts/stream-gates.enum';

@Component({
	selector: 'app-code-view',
	templateUrl: './code-view.component.html',
	styleUrls: ['./code-view.component.scss']
})
export class CodeViewComponent implements OnInit {

	StreamGate = StreamGate;

	@Input() inputValue: string;
	@Input() debounceProgress: number;
	@Input() debounceDelay: number;
	@Input() trimmed: string;
	@Input() distinct: string;
	@Input() distinctComparisons: { x: string, y: string, distinct: boolean };
	@Input() characterList: CharacterList;

	streamGate = null;

	@ViewChild('canvas') canvasRef: ElementRef;

	constructor() { }

	ngOnInit(): void { }


	calculateDashArray(progress: number): string {
		return `${progress * 360} , 20000`;
	}
}
