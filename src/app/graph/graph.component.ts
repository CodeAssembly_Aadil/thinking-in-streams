import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import * as Paper from 'paper';
import { CanvasSpace, Pt, Group, Space, CanvasForm, Rectangle } from 'pts';

@Component({
	selector: 'app-graph',
	templateUrl: './graph.component.html',
	styleUrls: ['./graph.component.scss']
})
export class GraphComponent implements AfterViewInit {

	// @ViewChild('canvas') canvasRef: ElementRef;

	private form: CanvasForm;

	constructor() { }

	ngAfterViewInit(): void {
		// const canvas: HTMLCanvasElement = this.canvasRef.nativeElement;
		// // Create an empty project and a view for the canvas:
		// Paper.setup(canvas);
		// // Create a Paper.js Path to draw a line into it:
		// const path: paper.Path = new Paper.Path();
		// // Give the stroke a color
		// path.strokeColor = 'black';
		// const start: Paper.Point = new Paper.Point(100, 100);
		// // Move to start and draw a line from there
		// path.moveTo(start);
		// // Note that the plus operator on Point objects does not work
		// // in JavaScript. Instead, we need to call the add() function:
		// path.lineTo(start.add([200, -50]));
		// // Draw the view now:
		// Paper.view.draw();

		// const space: CanvasSpace = new CanvasSpace(canvas);
		// this.form = space.getForm();
		// space.add((time, ftime) => {
		// 	this.form.fill('#f03').point(new Pt(120, 240), 30, 'circle');

		// 	this.form.fill('#f03').point(new Pt(240, 240), 30, 'circle');

		// 	this.form.fill('#f03').point(new Pt(360, 240), 30, 'circle');
		// 	this.form.fill('#f03').point(new Pt(480, 240), 30, 'circle');
		// 	this.form.fill('#f03').point(new Pt(600, 240), 30, 'circle');
		// 	this.form.fill('#f03').point(new Pt(720, 240), 30, 'circle');

		// 	this.form.strokeOnly('#f03', 5).line([new Pt(120, 240), new Pt(240, 240)]);
		// 	this.form.strokeOnly('#f03', 5).line([new Pt(240, 240), new Pt(360, 240)]);
		// 	this.form.strokeOnly('#f03', 5).line([new Pt(360, 240), new Pt(480, 240)]);
		// 	this.form.strokeOnly('#f03', 5).line([new Pt(480, 240), new Pt(600, 240)]);
		// 	this.form.strokeOnly('#f03', 5).line([new Pt(600, 240), new Pt(720, 240)]);
		// 	// this.form.strokeOnly("#f03", 5).line([new Pt(720, 240), new Pt(360, 240)]);
		// });
		// space.bindMouse().bindTouch().play();
	}

}
