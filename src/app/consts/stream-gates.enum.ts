export enum StreamGate {
	DEBOUNCE = 'DEBOUNCE',
	MAP = 'MAP',
	DISTINCT_UNTIL_CHANGED = 'DISTINCT_UNTIL_CHANGED',
	SWITCH_MAP = 'SWITCH_MAP',
}
