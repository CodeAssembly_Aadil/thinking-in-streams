import { Character } from './character.model';

export interface CharacterList {
  characters: Character[];
  basePath: string;
}
